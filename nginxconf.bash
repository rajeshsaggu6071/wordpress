#! /bin/bash
site_name=`cat data.txt `
domian=idslogic.net
cd /etc/php/8.1/fpm/pool.d/
path=/var/www/html/${site_name}.dev.${domian}
echo $site_name
useradd  -d $path --shell /bin/bash -g www-data ${site_name}
chown -R $site_name:www-data $path
echo 'creating fpm.sock file'
site=${site_name}.conf
cat www.conf > ${site}
sed -i "4 s/www/${site_name}/" ${site}
sed -i "23 s/www-data/${site_name}/" ${site}
sed -i "36 s/fpm.sock/fpm_${site_name}.sock/" ${site}
chown -R  www-data:www-data /run/php
service php8.1-fpm restart
echo 'making changes in nginx config file'
cd /etc/nginx/sites-available &&  mv wordpress.com ${site_name}.dev.${domian}
sed -i "s/wordpress/${site_name}/" /etc/nginx/sites-available/${site_name}.dev.${domian}
cd /etc/nginx/sites-enabled
ln -sf /etc/nginx/sites-available/${site_name}.dev.${domian}
nginx -t
service nginx restart
