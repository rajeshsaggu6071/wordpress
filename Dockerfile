FROM rajeshids/wordpress_custom:latest
WORKDIR /app
COPY nginxconf.bash .
COPY test.bash .
COPY data.txt  .
COPY wordpress.com /etc/nginx/sites-available/
RUN chmod +x /app/nginxconf.bash
Expose 80
CMD [ "bash" , "test.bash"]
