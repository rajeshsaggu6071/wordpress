upstream fastcgi_backend {

        server unix:/run/php/php8.1-fpm_wordpress.sock;
}


server {
            listen 80;
            root /var/www/html/wordpress.dev.idslogic.net/wordpress;
            index index.php;
            server_name wordpress.dev.idslogic.net;

            access_log /var/log/nginx/wordpress.access.log;
            error_log /var/log/nginx/wordpress.error.log;

            client_max_body_size 64M;

            location / {

                try_files $uri $uri/ /index.php?$args;
                auth_basic "Restricted Content";
                auth_basic_user_file /etc/nginx/.htpasswd;
            }

            location ~ \.php$ {
                         include snippets/fastcgi-php.conf;
                         fastcgi_pass unix:/run/php/php8.1-fpm_wordpress.sock;
            }

            location ~ /\.ht {
                         deny all;
            }

            location = /favicon.ico {
                         log_not_found off;
                         access_log off;
            }

            location = /robots.txt {
                         allow all;
                         log_not_found off;
                         access_log off;
           }

            location ~* \.(js|css|png|jpg|jpeg|gif|ico)$ {
                         expires max;
                         log_not_found off;
           }
}
